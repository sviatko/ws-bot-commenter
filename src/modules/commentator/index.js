import { facade } from "./src/Facade";
import * as config from "./src/config";

export const greetings = (socket, { commentatorName }) => {
  const data = {
    type: config.GREETINGS,
    inject: commentatorName,
  };

  run(socket, data);
};

export const participants = (socket, { participants }) => {
  const data = {
    type: config.LIST_PARTICIPANTS,
    inject: participants,
  };

  run(socket, data);
};

export const joke = (socket) => {
  const data = {
    type: config.JOKE,
  };

  run(socket, data);
};

export const closeToFinishLine = (socket, { leader }) => {
  const data = {
    type: config.CLOSE_TO_FINISH_LINE,
    inject: leader,
  };

  run(socket, data);
};

export const finishLine = (socket, { leaderList }) => {
  const data = {
    type: config.FINISH_LINE,
    inject: leaderList,
  };

  run(socket, data);
};

export const raceStatus = (socket, { participants }) => {
  const data = {
    type: config.RACE_STATUS,
    inject: participants,
  };

  run(socket, data);
};

export const sayBye = (socket, { name }) => {
  const data = {
    type: config.SAY_BYE,
    inject: name,
  };

  run(socket, data);
};

const run = (socket, data) => {
  const comment = facade.createComment(data);
  const type = data.type + config.SOCKET_POSTFIX;

  socket.emit(type, comment);
  socket.broadcast.emit(type, comment);
};
