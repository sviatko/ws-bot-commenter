// Comment type
export const GREETINGS = "GREETINGS";
export const LIST_PARTICIPANTS = "LIST_PARTICIPANTS";
export const JOKE = "JOKE";
export const RACE_STATUS = "RACE_STATUS";
export const CLOSE_TO_FINISH_LINE = "CLOSE_TO_FINISH_LINE";
export const FINISH_LINE = "FINISH_LINE";
export const SAY_BYE = "SAY_BYE";

// String to search
export const SEARCH_FOR_NAME = "%name%";
export const SEARCH_FOR_CAR = "%car%";
export const SEARCH_FOR_LIST = "%list%";

// Socket config
export const SOCKET_POSTFIX = "_DONE";
