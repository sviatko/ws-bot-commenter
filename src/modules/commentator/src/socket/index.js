import * as config from "../config";
import * as reducers from "../../index";

export default (io) => {
  io.on("connection", (socket) => {
    socket.on(config.GREETINGS, (data) => reducers.greetings(socket, data));

    socket.on(config.LIST_PARTICIPANTS, (data) =>
      reducers.participants(socket, data)
    );

    socket.on(config.JOKE, (data) => reducers.joke(socket, data));

    socket.on(config.RACE_STATUS, (data) => reducers.raceStatus(socket, data));

    socket.on(config.CLOSE_TO_FINISH_LINE, (data) =>
      reducers.closeToFinishLine(socket, data)
    );

    socket.on(config.FINISH_LINE, (data) => reducers.finishLine(socket, data));

    socket.on(config.SAY_BYE, (data) => reducers.sayBye(socket, data));
  });
  io.on("disconnect", (socket) => {});
};
