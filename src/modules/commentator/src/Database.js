import data from "./data.json";

class Database {
  constructor() {
    this.instance;
  }

  createInstance() {
    return data;
  }

  getInstance() {
    if (!this.instance) {
      this.instance = this.createInstance();
    }

    return this.instance;
  }
}

export const database = new Database();
