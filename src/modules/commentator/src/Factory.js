import { SEARCH_FOR_NAME, SEARCH_FOR_LIST } from "./config";
import { participantsListString } from "./helper";

class Factory {
  injectStringToTemplate(templateString) {
    return (stringToInject) =>
      templateString.replace(SEARCH_FOR_NAME, stringToInject);
  }

  injectStringAndListToTemplate(templateList) {
    return (templateString) => (list) => {
      const listStr = participantsListString(list, templateString);

      return templateList.replace(SEARCH_FOR_LIST, listStr);
    };
  }
}

export const factory = new Factory();
