import { database } from "./Database";

class Proxy {
  constructor() {
    this.cache = {};
    this.databaseInstance = database.getInstance();
  }

  checkForCache(type) {
    if (!this.cache[type]) {
      this.cache[type] = this.databaseInstance[type];
    }
  }

  greetings() {
    this.checkForCache("greetings");

    return this.cache.greetings;
  }

  listParticipants() {
    this.checkForCache("listParticipants");

    return this.cache.listParticipants;
  }

  participant() {
    this.checkForCache("participant");

    return this.cache.participant;
  }

  joke() {
    this.checkForCache("joke");

    return this.cache.joke;
  }

  closeTofinishLine() {
    this.checkForCache("closeTofinishLine");

    return this.cache.closeTofinishLine;
  }

  finishLine() {
    this.checkForCache("finishLine");

    return this.cache.finishLine;
  }

  sayBye() {
    this.checkForCache("sayBye");

    return this.cache.sayBye;
  }

  raceStatus() {
    this.checkForCache("raceStatus");

    return this.cache.raceStatus;
  }
}

export const proxy = new Proxy();
