import { SEARCH_FOR_NAME, SEARCH_FOR_CAR } from "./config";

export const participantsListString = (participantsArray, participantString) =>
  participantsArray
    .map(({ name, car }) =>
      participantString
        .replace(SEARCH_FOR_NAME, name)
        .replace(SEARCH_FOR_CAR, car)
    )
    .join(", ");
