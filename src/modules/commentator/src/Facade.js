import { proxy } from "./Proxy";
import { factory } from "./Factory";
import * as config from "./config";

class Facade {
  createComment({ type, inject }) {
    let comment = "";
    let stringParticipantsToInject;
    let stringParticipant;
    let participantsArray;

    switch (type) {
      case config.GREETINGS:
        const greetingsString = proxy.greetings();

        comment = factory.injectStringToTemplate(greetingsString)(inject);
        break;
      case config.LIST_PARTICIPANTS:
        stringParticipantsToInject = proxy.listParticipants();
        stringParticipant = proxy.participant();
        participantsArray = [...inject];

        comment = factory.injectStringAndListToTemplate(
          stringParticipantsToInject
        )(stringParticipant)(participantsArray);
        break;
      case config.JOKE:
        comment = proxy.joke();
        break;
      case config.RACE_STATUS:
        stringParticipantsToInject = proxy.raceStatus();
        stringParticipant = proxy.participant();
        participantsArray = [...inject];

        comment = factory.injectStringAndListToTemplate(
          stringParticipantsToInject
        )(stringParticipant)(participantsArray);
        break;
      case config.CLOSE_TO_FINISH_LINE:
        const closeTofinishLineString = proxy.closeTofinishLine();

        comment = factory.injectStringToTemplate(closeTofinishLineString)(
          inject
        );
        break;
      case config.FINISH_LINE:
        const stringLeaderToInject = proxy.finishLine();
        const stringParticipantLeader = proxy.participant();
        const leaderArray = [...inject];

        comment = factory.injectStringAndListToTemplate(stringLeaderToInject)(
          stringParticipantLeader
        )(leaderArray);
        break;
      case config.SAY_BYE:
        const sayByeString = proxy.sayBye();

        comment = factory.injectStringToTemplate(sayByeString)(inject);
        break;
      default: {
        const greetingsString = proxy.greetings();

        comment = factory.injectStringToTemplate(greetingsString);
      }
    }

    return comment;
  }
}

export const facade = new Facade();
