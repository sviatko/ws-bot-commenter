import loginRoutes from "./loginRoutes";
import gameRoutes from "./gameRoutes";
import commentRoutes from "./commentRoutes";

export default (app) => {
  app.use("/login", loginRoutes);
  app.use("/game", gameRoutes);
  app.use("/comment", commentRoutes);
};
