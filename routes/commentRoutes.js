import { Router } from "express";

import {
  greetings,
  participants,
  closeToFinishLine,
  joke,
  finishLine,
  sayBy,
  raceStatus,
} from "../src/modules/commentator";

const router = Router();

router.post("/greetings", (req, res) => {
  const name = req.body.name;
  const comment = greetings(name);

  res.json({ status: 200, comment: comment });
});

router.post("/participants", (req, res) => {
  const participantsList = req.body.participants;
  const comment = participants(participantsList);

  res.json({ status: 200, comment: comment });
});

router.post("/joke", (req, res) => {
  const comment = joke();

  res.json({ status: 200, comment: comment });
});

router.post("/close-to-finish-line", (req, res) => {
  const leader = req.body.leader;
  const comment = closeToFinishLine(leader);

  res.json({ status: 200, comment: comment });
});

router.post("/finish-line", (req, res) => {
  const leaderList = req.body.leaderList;
  const comment = finishLine(leaderList);

  res.json({ status: 200, comment: comment });
});

router.post("/race-status", (req, res) => {
  const leaderList = req.body.leaderList;
  const comment = raceStatus(leaderList);

  res.json({ status: 200, comment: comment });
});

router.post("/say-bye", (req, res) => {
  const name = req.body.name;
  const comment = sayBye(name);

  res.json({ status: 200, comment: comment });
});

export default router;
