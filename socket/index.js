import game from "./game";
import commentBot from "../src/modules/commentator/src/socket/index";

export default (io) => {
  game(io.of("game"));
  commentBot(io.of("comment-bot"));
};
