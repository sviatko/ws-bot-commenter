import {
  createElement,
  addClass,
  removeClass,
  getPlayersObject,
  compareByProgress,
  setRandomCarForPlayers,
} from "../helper.mjs";
import { playerListRender } from "./player.module.mjs";
import { insertCommentWindow } from "./comment-bot.module.mjs";
import {
  STATUS_CHECK_SECONDS,
  DIGITS_TO_FINISH_LINE,
  FINISH_LINE,
  SECONDS_FOR_JOKE,
} from "./constants.mjs";
import { getRooms } from "../helper.mjs";
import { setRooms } from "../helper.mjs";

const socket = io("http://localhost:3002/game");
const socketComentBot = io("http://localhost:3002/comment-bot");
const username = sessionStorage.getItem("username");
const commentatorName = "John Doe";

let workingText = "";

export const createRoom = () => {
  let rooms = [];
  if (window.localStorage.getItem("rooms")) {
    rooms = getRooms();
  }

  const roomName = prompt("Please enter room name", "Room name #1");

  if (!roomName) {
    alert("Room name cannot be empty");
    return false;
  }
  if (rooms.find((room) => room.roomName === roomName)) {
    alert("Duplicated room name");
    return false;
  }

  roomRender(roomName);

  insertCommentWindow();

  socket.emit("ROOM_CREATED", { roomName, username });
};

export const backToRooms = (e) => {
  const roomId = document.querySelector("#room-name").innerText;
  homepageRender();
  socket.emit("ROOM_LEAVE", { roomId, username });
};

export const roomListUpdate = ({ rooms }) => {
  const roomsListElement = document.querySelector("#room-list");
  roomsListElement.innerHTML = "";

  setRooms(rooms);

  Object.keys(rooms).forEach((roomIndex) => {
    const room = rooms[roomIndex];

    const roomElement = createElement({
      tagName: "div",
      className: "room-item border",
      attributes: { id: room.roomName },
    });
    const spanTotalElement = createElement({
      tagName: "span",
      className: "room-total d-block",
    });
    const spanElement = createElement({
      tagName: "span",
      className: "room-title",
    });
    const roomJoinElement = createElement({
      tagName: "button",
      className: "btn btn-light js-join-room",
    });

    roomJoinElement.innerText = "Join";
    spanElement.innerText = room.roomName;
    spanTotalElement.innerText = `${Object.keys(room.players).length} players`;

    roomElement.appendChild(spanTotalElement);
    roomElement.appendChild(spanElement);
    roomElement.appendChild(roomJoinElement);
    roomsListElement.appendChild(roomElement);

    roomJoinElement.addEventListener("click", joinToRoom);

    setRandomCarForPlayers(room.roomName);
  });
};

export const joinToRoom = (e) => {
  const roomName = e.target.closest(".room-item").querySelector(".room-title")
    .innerText;

  socket.emit("ROOM_JOIN", { roomId: roomName, username });
};

const roomRender = (roomName) => {
  const gamePage = document.querySelector("#game-page");
  const roomsPage = document.querySelector("#rooms-page");
  const roomNameElement = document.querySelector("#room-name");
  const readyBtn = document.querySelector(".js-game-ready");

  removeClass(gamePage, "display-none");
  addClass(roomsPage, "display-none");

  roomNameElement.innerText = roomName;

  const playerReadyListener = () => {
    socket.emit("ROOM_PLAYER_READY", { roomId: roomName, username });
  };

  readyBtn.addEventListener("click", playerReadyListener);
};

const homepageRender = () => {
  const gamePage = document.querySelector("#game-page");
  const roomsPage = document.querySelector("#rooms-page");

  removeClass(roomsPage, "display-none");
  addClass(gamePage, "display-none");
};

const joinToRoomDone = ({ roomName, players }) => {
  roomRender(roomName);
  playerListRender({ players });
  insertCommentWindow();
};

const errorAlert = ({ message }) => alert(message);

const startCountDown = ({ timer, text, gameTimer }) => {
  const gameContent = document.querySelector(".game-table");

  const countDown = window.setInterval(() => {
    gameContent.innerHTML = timer;
    if (timer < 1) {
      clearInterval(countDown);

      startGame(text, gameTimer);
    }
    timer--;
  }, 1000);
};

const startGame = (text, gameTimer) => {
  workingText = text;

  const roomId = document.querySelector("#room-name").innerText;
  const players = getPlayersObject(roomId);

  socketComentBot.emit("LIST_PARTICIPANTS", {
    roomId,
    participants: players,
  });

  const charsLeft = workingText
    .split("")
    .map((char) => (char === " " ? "&nbsp;" : char));
  const charActive = charsLeft[0];

  const gameContent = document.querySelector(".game-table");
  gameContent.innerHTML = "";

  const timerElement = createElement({
    tagName: "span",
    className: "game-timer",
  });

  timerElement.innerText = `${gameTimer} seconds left`;

  let counter = 0;
  const gameInterval = setInterval(() => {
    gameTimer--;
    counter++;
    timerElement.innerText = `${gameTimer} seconds left`;
    if (gameTimer === 0) {
      clearInterval(gameInterval);
      socket.emit("GAME_FINISHED", { roomId });
    }

    if (counter === STATUS_CHECK_SECONDS) {
      counter = 0;
      const players = getPlayersObject(roomId);

      socketComentBot.emit("RACE_STATUS", {
        roomId,
        participants: players.sort(compareByProgress),
      });
    }
    if (counter === SECONDS_FOR_JOKE) {
      socketComentBot.emit("JOKE");
    }
  }, 1000);

  const typedCharsElement = createElement({
    tagName: "span",
    className: "typed-chars",
  });
  const activeCharElement = createElement({
    tagName: "span",
    className: "active-char",
  });
  const charsLeftElement = createElement({
    tagName: "span",
    className: "chars-left",
  });

  charsLeft.shift();

  charsLeftElement.innerHTML = charsLeft.join("");
  activeCharElement.innerHTML = charActive;

  gameContent.append(
    timerElement,
    typedCharsElement,
    activeCharElement,
    charsLeftElement
  );

  document.addEventListener("keypress", keyPressListener);
};

let keyPressedCounter = 0;
let progress = 0;
const keyPressListener = (event) => {
  const roomId = document.querySelector("#room-name").innerText;
  const gameContent = document.querySelector(".game-table");
  const typedCharsElement = gameContent.querySelector(".typed-chars");
  const activeCharElement = gameContent.querySelector(".active-char");
  const charsLeftElement = gameContent.querySelector(".chars-left");

  const typedChars = typedCharsElement.innerText.split("");
  const charsLeft = charsLeftElement.innerText.split("");
  let activeChar = activeCharElement.innerText;

  const key = event.key;

  if (key.trim() === activeChar.trim()) {
    keyPressedCounter++;

    typedChars.push(activeChar);
    activeChar = charsLeft[0];

    charsLeft.shift();

    typedCharsElement.innerText = typedChars.join("");
    charsLeftElement.innerText = charsLeft.join("");

    if (activeChar) {
      activeCharElement.innerText = activeChar;
    } else {
      activeCharElement.innerText = "";
    }

    progress = (keyPressedCounter * 100) / workingText.length;

    socket.emit("PLAYER_PROGRESS", { roomId, username, progress });

    const players = getPlayersObject(roomId);

    const digitsTotalInText = workingText.length;
    const leaderList = players.sort(compareByProgress);

    switch (digitsTotalInText - keyPressedCounter) {
      case DIGITS_TO_FINISH_LINE:
        socketComentBot.emit("CLOSE_TO_FINISH_LINE", {
          leader: leaderList[0].name,
        });
        break;

      case FINISH_LINE:
        socketComentBot.emit("FINISH_LINE", {
          leaderList,
        });
        break;
    }
  }
};

const finishGame = ({ winners, players }) => {
  const results = [];
  const game = document.querySelector(".game-table");

  game.innerHTML = "";
  addClass(game, "flex-column");

  winners.forEach((win) => {
    results.push({ name: win, progress: 100 });
  });
  players.forEach((p) => {
    const pName = Object.keys(p)[0];
    if (!winners.includes(pName)) {
      results.push({ name: pName, progress: 0 });
    }
  });

  const header = createElement({ tagName: "h2" });
  header.innerText = "Good job!";

  game.append(header);

  results.forEach((r, index) => {
    const resSpan = createElement({ tagName: "span" });
    resSpan.innerText = `${index + 1}# ${r.name}`;

    game.append(resSpan);
  });

  document.removeEventListener("keypress", keyPressListener);
  socketComentBot.emit("SAY_BYE", {
    name: commentatorName,
  });
};

socket.on("JOIN_ROOM_DONE", joinToRoomDone);
socket.on("ROOM_PLAYERS_LIST_UPDATE", playerListRender);
socket.on("ROOMS_LIST_UPDATE", roomListUpdate);
socket.on("ROOMS_ALERT", errorAlert);
socket.on("GAME_START_COUNT_DOWN", startCountDown);
socket.on("GAME_FINISHED_DONE", finishGame);
