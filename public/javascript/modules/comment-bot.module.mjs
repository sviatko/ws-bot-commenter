import { createElement } from "../helper.mjs";

const socket = io("http://localhost:3002/comment-bot");
const commentatorName = "John Doe";

export const insertCommentWindow = () => {
  const commentWindow = createElement({
    tagName: "div",
    className: "comment-window",
  });
  const rootElement = document.querySelector("#root");
  const roomName = document.querySelector("#room-name");

  rootElement.append(commentWindow);
  socket.emit("GREETINGS", { roomName, commentatorName });
};

const insertComment = (comment) => {
  const commentWindow = document.querySelector(".comment-window");
  commentWindow.innerHTML = comment;
};

socket.on("GREETINGS_DONE", insertComment);
socket.on("LIST_PARTICIPANTS_DONE", insertComment);
socket.on("RACE_STATUS_DONE", insertComment);
socket.on("CLOSE_TO_FINISH_LINE_DONE", insertComment);
socket.on("FINISH_LINE_DONE", insertComment);
socket.on("JOKE_DONE", insertComment);
socket.on("SAY_BYE_DONE", insertComment);
