export const STATUS_CHECK_SECONDS = 30;

export const SECONDS_FOR_JOKE = 20;

export const DIGITS_TO_FINISH_LINE = 30;

export const FINISH_LINE = 10;

export const CARS = ["Audi", "BMW", "Volvo", "Mercedes"];
