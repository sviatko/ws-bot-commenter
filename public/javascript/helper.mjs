import { CARS } from "./modules/constants.mjs";

export const createElement = ({ tagName, className, attributes = {} }) => {
  const element = document.createElement(tagName);

  if (className) {
    addClass(element, className);
  }

  Object.keys(attributes).forEach((key) =>
    element.setAttribute(key, attributes[key])
  );

  return element;
};

export const addClass = (element, className) => {
  const classNames = formatClassNames(className);
  element.classList.add(...classNames);
};

export const removeClass = (element, className) => {
  const classNames = formatClassNames(className);
  element.classList.remove(...classNames);
};

export const formatClassNames = (className) =>
  className.split(" ").filter(Boolean);

export const getPlayersObject = (room) => {
  const currentRoom = findRoom(room).room;
  const players = currentRoom.players;

  return players.map((p) => {
    const name = Object.keys(p)[0];
    const player = { ...p[name] };
    player.name = name;

    return player;
  });
};

export const getRooms = () => JSON.parse(window.localStorage.getItem("rooms"));

export const setRooms = (data) =>
  window.localStorage.setItem("rooms", JSON.stringify(data));

export const findRoom = (room) => {
  let roomIndex = 0;
  const rooms = getRooms();
  const currentRoom = rooms.find((r, index) => {
    if (r.roomName === room) {
      roomIndex = index;
      return true;
    }

    return false;
  });
  return { room: currentRoom, indexInLocalStorage: roomIndex };
};

export const compareByProgress = (playerOne, playerTwo) => {
  if (playerOne.progress < playerTwo.progress) {
    return 1;
  }
  if (playerOne.progress > playerTwo.progress) {
    return -1;
  }
  return 0;
};

export const setRandomCarForPlayers = (roomName) => {
  const roomObj = findRoom(roomName);
  const room = roomObj.room;
  const index = roomObj.indexInLocalStorage;
  const players = [...room.players];
  let rooms = getRooms();

  room.players = players.map((p) => {
    const name = Object.keys(p)[0];
    console.log();
    // const

    return {
      [name]: {
        ...p[name],
        car: CARS[getRandomInt(3)],
      },
    };
  });

  rooms[index] = room;

  setRooms(rooms);
};

export const getRandomInt = (max = 3, min = 0) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

export const getRandomCar = () => {
  const max = CARS.length;
  const random = getRandomInt(max);

  return CARS[random];
};
