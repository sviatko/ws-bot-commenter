import { playersTotalUpdate } from "./modules/player.module.mjs";
import {
  createRoom,
  backToRooms,
  roomListUpdate,
} from "./modules/room.module.mjs";

const username = sessionStorage.getItem("username");
const socket = io("http://localhost:3002/game");

if (!username) {
  window.location.replace("/login");
}

const createRoomBtn = document.querySelector(".js-create-room");
const backToRoomsBtn = document.querySelector(".js-back-to-rooms");

createRoomBtn.addEventListener("click", createRoom);
backToRoomsBtn.addEventListener("click", backToRooms);

socket.on("ROOMS_INCREASE_TOTAL", playersTotalUpdate);
socket.on("ROOMS_LIST_UPDATE", roomListUpdate);
